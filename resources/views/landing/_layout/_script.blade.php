<!--   Core JS Files   -->
<script src="{{ URL::asset('landing/js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('landing/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('landing/js/material.min.js') }}"></script>

<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{ URL::asset('landing/js/nouislider.min.js') }}" type="text/javascript"></script>

<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
<script src="{{ URL::asset('landing/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>

<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
<script src="{{ URL::asset('landing/js/material-kit.js') }}" type="text/javascript"></script>

<!-- Sweetalert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.js"></script>

<script type="text/javascript">

    $().ready(function(){
            // the body of this function is in landing/material-kit.js
            materialKit.initSliders();
            window_width = $(window).width();

            if (window_width >= 992){
                big_image = $('.wrapper > .header');

                $(window).on('scroll', materialKitDemo.checkScrollForParallax);
            }

        });
    </script>