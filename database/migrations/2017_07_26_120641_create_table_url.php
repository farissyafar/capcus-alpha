<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('url');
            $table->string('code')->nullable()->unique();
            $table->string('alias')->nullable()->unique();
            $table->text('desc')->nullable();
            $table->string('registrar')->nullable();
            $table->datetime('valid_until')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
        $statement = "ALTER TABLE links AUTO_INCREMENT = 1000;";
        DB::unprepared($statement);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
