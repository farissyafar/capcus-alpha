<!-- Navbar -->
    <nav class="navbar navbar-info navbar-transparent navbar-fixed-top navbar-color-on-scroll">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-index">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="http://www.creative-tim.com">
                    <div class="logo-container">
                        <div class="logo">
                            <img src="{{ URL::asset('landing/img/logo.png') }}" alt="Creative Tim Logo" rel="tooltip" title="<b>Material Kit</b> was Designed & Coded with care by the staff from <b>Creative Tim</b>" data-placement="bottom" data-html="true">
                        </div>
                        <div class="brand">
                            Creative Tim
                        </div>
                    </div>
                </a>
            </div>

            <div class="collapse navbar-collapse" id="navigation-index">
                <ul class="nav navbar-nav navbar-right">
                    @if (Route::has('login'))
                        @if (Auth::check())
                            <li>
                                <a href="{{ url('/home') }}">
                                    <i class="material-icons">dashboard</i> Home
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="{{ url('/login') }}">
                                    <i class="material-icons">unarchive</i> Login
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/register') }}">
                                    <i class="material-icons">unarchive</i> Register
                                </a>
                            </li>
                        @endif
                    @endif
                    {{-- <li>
                        <a href="components-documentation.html" target="_blank">
                            <i class="material-icons">dashboard</i> Components
                        </a>
                    </li>
                    <li>
                        <a href="http://demos.creative-tim.com/material-kit-pro/presentation.html?ref=utp-freebie" target="_blank">
                            <i class="material-icons">unarchive</i> Upgrade to PRO
                        </a>
                    </li>
                    <li>
                        <a rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank" class="btn btn-white btn-simple btn-just-icon">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank" class="btn btn-white btn-simple btn-just-icon">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                    </li>
                    <li>
                        <a rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank" class="btn btn-white btn-simple btn-just-icon">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li> --}}

                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->