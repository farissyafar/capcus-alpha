<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/awal', function () {
    return view('welcome');
});

Route::get('/', array(
	'as' => 'home',
	'uses' => 'LinksController@index'
));

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/make', array(
	'as' => 'make',
	'uses' => 'LinksController@make'
));
Route::get('/{code}', array(
	'as' => 'get',
	'uses' => 'LinksController@get'
));
Route::get('/dashboard/template', function () {
    return view('dashboard/index');
});
Route::get('/landing/template', function () {
    return view('landing/index');
});