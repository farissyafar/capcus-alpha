<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class Link extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url', 'code', 'registrar', 'valid_until', 'updated_by', 
    ];

    /**
     * Method to check if the URL is exist in database.
     *
     * @param string $url Checked URL.
     * @return boolean Is URL Exist.
     */
    public static function isURLExist($url){
        $exists = self::where('url', '=', $url);
        if ($exists->count() === 1) {
            return true;
        }
        return false;
    }

    /**
     * Method to generate shorten URL.
     *
     * @param string $url URL.
     * @return string Shorten URL generated.
     */
    public static function createShortenURL($url){
        $created = self::create(array(
            'url' => $url,
            'created_at' => new DateTime()
        ));
        if ($created) {
            $code = base_convert($created->id, 10, 36);

            self::where('id', '=', $created->id)->update(array(
                'code' => $code,
                'registrar' => NULL,
                'updated_at' => new DateTime(),
                'valid_until' => new DateTime('1st January 2999'),
                'updated_by' => NULL
            ));
        }
        return $code;
    }

    /**
     * Method to get URL by shorten Code.
     *
     * @param string $code Shorten Code.
     * @return string Origin URL.
     */
    public static function getURLbyCode($code){
        $exists = self::where('code', '=', $code);
        return $exists->first()->url;
    }

    /**
     * Method to get Code by URL.
     *
     * @param string $url URL.
     * @return string Shorten Code.
     */
    public static function getCodeByURL($url){
        $exists = self::where('url', '=', $url);
        return $exists->first()->code;
    }
}
