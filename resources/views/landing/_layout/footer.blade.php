<footer class="footer" style="padding: 7px 0 0px 0;">
    <div class="container">
        <nav class="pull-left">
            <ul>
                <li>
                    <a href="#">
                        Vesten
                    </a>
                </li>
                <li>
                    <a href="#">
                       About Us
                   </a>
               </li>
               <li>
                <a href="#">
                  Blog
                </a>
              </li>
              <li>
                <a href="#">
                  Terms
                </a>
              </li>
    </ul>
</nav>
<div class="copyright pull-right">
    &copy; 2017, made with <i class="fa fa-heart-o"></i>  by Vesten.
</div>
</div>
</footer>