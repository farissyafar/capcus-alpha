<?php
    use Illuminate\Support\Facades\Input;
?>

@extends('landing._layout.basic')
@section('content')
<div class="header header-filter" style="background-image: url('{{ URL::asset('landing/img/bg2.jpeg') }}');">
    <div class="container">
        <div class="row mb15">
            <div class="col-md-8 col-md-offset-2">
                <div class="brand">
                    <h1>NGE.LINK</h1>
                    <h4>A Badass URL Shortener for everyone</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="card card-raised card-form-horizontal">
                    <div class="card-content">
                        <div class="row">
                        <form action="{{ URL::route('make') }}" method="post">
                            <div class="col-md-9">
                                <div class="form-group form-info label-floating is-empty">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input name="url" type="url" placeholder="Place your URL here" class="form-control" required="true" autocomplete="on"{{ Input::old('url') ? ' value '.e(Input::old('url')).'"':''}}>
                                    <span class="material-input"></span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-info btn-block">
                                    Shorten
                                </button>
                            </div>
                            <div class="col-xs-12 text-center">
                                @if($errors->has('url'))
                                <div class="alert alert-warning mt30">
                                    <div class="container-fluid">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                        </button>
                                        <b>Alert: </b>{{ $errors->first('url') }}
                                    </div>
                                </div>
                                @endif
                                @if(!empty($shorten))
                                <div class="alert alert-info mt10 mb0">
                                    <div class="container-fluid">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                        </button>
                                        <b>This is your shorten URL: </b>{!! $shorten !!}
                                        <br>
                                        <a href="mailto:?Subject=Short URL about&amp;Body={{ $link }}" class="btn btn-simple btn-just-icon">
                                            <i class="fa fa-envelope-o"></i>
                                        </a>
                                        <a href="http://www.facebook.com/sharer.php?u={{ $link }}" target="_blank" class="btn btn-simple btn-just-icon">
                                            <i class="fa fa-facebook-square"></i>
                                        </a>
                                        <a href="https://twitter.com/share?url={{ $link }}&amp;text={{ $link }}" target="_blank" class="btn btn-simple btn-just-icon">
                                            <i class="fa fa-twitter-square"></i>
                                        </a>
                                        <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ $link }}" target="_blank" class="btn btn-simple btn-just-icon">
                                            <i class="fa fa-linkedin-square"></i>
                                        </a>
                                        <a href="whatsapp://send?text={{ $link }}" data-action="share/whatsapp/share" class="btn btn-simple btn-just-icon">
                                            <i class="fa fa-whatsapp"></i>
                                        </a>
                                        <a id="copyTo" href="#" class="btn btn-simple btn-just-icon">
                                            <i class="fa fa-copy"></i>
                                        </a>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('pagescript')
    <script type="text/javascript">
        $('#copyTo').click(function(){
            copyToClipboard('#lblShorten');
            swal(
                'Copied!',
                'Now you can paste it anywhere.',
                'success'
              )
        });

        function copyToClipboard(element) {
          var $temp = $("<input>");
          $("body").append($temp);
          $temp.val($(element).text()).select();
          document.execCommand("copy");
          $temp.remove();
        }
    </script>
@endsection