<!doctype html>
<html lang="{{ config('app.locale') }}">

@include('landing._layout._head')

<body class="index-page">
    @include('landing._layout.navbar')
    <div class="wrapper">
        @yield('content')
        @include('landing._layout.footer')
    </div>
</body>
    @include('landing._layout._script')
    @yield('pagescript')
</html>