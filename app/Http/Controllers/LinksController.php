<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\HTML;
use App\Models\Link;
use DateTime;

class LinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('landing/home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function make() 
    {
        $validator = Validator::make(Input::all(), array(
            'url' => 'required|url|max:255'
        ));
        if ($validator->fails()) {
            return redirect()->route('home')->withInput()->withErrors($validator);
        } else {
            $url = Input::get('url');
            $code = null;
            
            if (Link::isURLExist($url)) {
                $code = Link::getCodeByURL($url);
            } else {
                $code = Link::createShortenURL($url);
            }
            if ($code) {
                $generatedURL = '<a id="lblShorten" href="' 
                . URL::route('get', $code) . '">' 
                . URL::route('get', $code) . '</a>';
                return view('landing/home')
                    ->with('shorten', $generatedURL)
                    ->with('link', urlencode(URL::route('get', $code))
                );
            }
            else {
                return redirect()->route('home')->with('global', 'Something went wrong, try again.');
            }
            
        }
        
    }

    public function get($code)
    {
        $link = Link::where('code', '=' , $code);  

        if ($link->count() === 1) {
            return redirect($link->first()->url);
        }

        return redirect()->route('home');
    }
}
